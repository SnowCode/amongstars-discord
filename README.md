# Along Among The Stars: Discord bot
> Find the original game [here](https://noroadhome.itch.io/alone-among-the-stars)

![demo screenshot](./demo.png)

This is a Python Discord bot. 

## Use the bot
I don't publish a simple link yet because it's not stable enough, but you can use the bot on [this server](https://discord.gg/8bQmePhzef)

## Features
* Solo playing of "Alone Among the Stars"
* Ability to burn the ship log
* Ability to log new entries
* Ability to generate a new planet
* Ability to backup the data
* Ability to read the full log of a user
* Ability to use the bot in private messages
* Designed for self-hosting easily
* Minimal code, very simple to understand, even for beginners

## Install dependencies

1. Install python3 and pip3 (debian-based only)

```bash
sudo apt install python3-pip python3
```

2. Install discord.py library

```bash
pip3 install discord.py
```

### How to get the Discord token 
1. Create an application on Discord dev portal [here](https://discord.com/developers/applications)
2. Select `Bot` on the left menu and create one. 
3. Select `OAuth` on the left menu, check `Bot`, then check `Administrator`, then copy and open the link in your browser to add the bot to a server.
4. Go back on the `Bot` tab and copy the token on the top of the page. 

## Installing

1. Enter root mode

```
sudo su
```

2. CLone the repository in /opt

```
cd /opt
git clone https://codeberg.org/SnowCode/amongstars-discord
cd amongstars-discord
```

3. Create a file called token.txt and paste your Discord token inside

```
nano token.txt
```

4. Run the script to test if it works properly. The bot should be up. If no errors are shown, press CTRL+C to kill it.

```
python3 bot.py
```

5. Create a systemd service, start it and enable it

```
cp amongstars.service /etc/systemd/system/
systemctl start amongstars
systemctl enable amongstars
systemctl status amongstars
```

## Usage
| Command | Meaning |
| --- | --- |
| `>land` | Find a new planet and land on it | 
| `>discover` | Discover a new element of the planet |
| `>log <your entry>` | Add the description of the element and your feelings into the log |
| `>burn` | Burn your log |
| `>read` | Read your log |
